GIT=git
CP=cp
PYTHON3 = python3

PROJECT_FOLDER = ./Dreamrider
SCRIPT_FOLDER = $(PROJECT_FOLDER)/Data/Scripts

LIB_FOLDER = Lib
# -----------------------------------------------------------------------------
#                                DEPENDENCIES
# -----------------------------------------------------------------------------
$(LIB_FOLDER):
	mkdir $(LIB_FOLDER)
	
MUTAGEN_FOLDER = $(LIB_FOLDER)/mutagen
$(MUTAGEN_FOLDER): $(LIB_FOLDER)
	cd $(LIB_FOLDER); \
	$(GIT) clone https://github.com/quodlibet/mutagen.git --depth=1;
mutagen: $(MUTAGEN_FOLDER)
	cd $(MUTAGEN_FOLDER); \
	$(GIT) pull; \
	$(GIT) checkout master;
	$(CP) -r $(MUTAGEN_FOLDER)/mutagen $(SCRIPT_FOLDER)
	$(CP) $(MUTAGEN_FOLDER)/COPYING $(SCRIPT_FOLDER)/mutagen

	
fetch-deps: mutagen
    