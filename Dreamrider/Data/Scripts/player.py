import math
import time
import random

import bge
import mathutils

import bank
import trail

MAPPING = {
    'left': 'AKEY',
    'right': 'DKEY',
    'boost': 'SPACEKEY'
}

FORCE_MULTIPLIER = 100  # Higher = faster vehicle


def get_key(action_name):
    '''Returns the input for a given action'''
    key_str = MAPPING[action_name]
    key_code = bge.events.__dict__[key_str]
    return bge.logic.keyboard.events[key_code] != 0


class Player(object):
    '''The vehicle the player drives'''
    def __init__(self, player_obj):
        self.obj = player_obj
        self.speed = 1
        self.trail = trail.Trail(self.obj.children['TRAIL'])

        self.on_hit = list()
        self.on_move = list()

        self.shield = bank.Bank(4, 0, 4)
        self.camera = Camera(self.obj.children['Camera'])
        self.on_move.append(self.camera.update)

        self.obj.collisionCallbacks.append(self.cb_hit)
        self.trail.length = 1.4
        self.hit_time = time.time()

    def update(self):
        '''Do player input'''
        self.speed += 0.0002 # +1 every 83 seconds
        self.trail.update()

        thrust = mathutils.Vector([0, 1, 0])
        thrust.x += get_key('right')
        thrust.x -= get_key('left')
        thrust.length = self.speed * FORCE_MULTIPLIER

        boost = get_key('boost')
        thrust.length *= (1 + boost * 2.0)

        self.obj.applyForce(thrust)

        drag = self.obj.worldLinearVelocity.copy()
    
        vel = self.obj.worldLinearVelocity
        ori = [0, 0, 0]
        ang = math.atan2(vel.x, vel.y)
        ori[1] = ang
        ori[2] = -ang
        self.obj.worldOrientation = ori

        for funct in self.on_move:
            funct(self, boost)
        
        self.obj.worldAngularVelocity *= 0.98
        self.obj.worldLinearVelocity *= 0.96

    def cb_hit(self, hit_obj):
        '''Runs when the player hits something'''
        if time.time() - self.hit_time > 0.5:
            self.shield.transaction(-1)
            for funct in self.on_hit:
                funct(self)
            self.hit_time = time.time()

    @property
    def transform(self):
        '''Where the player is'''
        return self.obj.worldTransform

    @property
    def current_speed(self):
        '''How fast the player is moving'''
        return self.obj.worldLinearVelocity.length


class Camera(object):
    def __init__(self, camera_obj):
        self.obj = camera_obj
        self.offset = camera_obj.localPosition.copy()
        self.fov = self.obj.fov
        self.obj.scene.active_camera = self.obj

        self.prev_pos = self.obj.worldPosition.copy()

        self.boost_lag = 0

    def update(self, _player, boost):
        self.boost_lag = boost * 0.05 + self.boost_lag * 0.95
        
        target_fov = self.fov - boost*15
        self.obj.fov = self.obj.fov * 0.9 + target_fov * 0.1

        target_position = self.offset + self.obj.getAxisVect([0, 0, 1]) * boost * 3
        self.obj.localPosition = self.obj.localPosition.lerp(target_position, 0.1)

        self.obj.worldPosition.x = self.obj.worldPosition.x * 0.8 + self.prev_pos.x * 0.2
        self.prev_pos = self.obj.worldPosition.copy()

        cam_shake = max(0, boost - self.boost_lag)
        self.obj.worldPosition.x += cam_shake * (random.random() - 0.5) * 0.2
        self.obj.worldPosition.z += cam_shake * (random.random() - 0.5) * 0.2