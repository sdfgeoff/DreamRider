'''
300 bu/second at 60FPS -> 5bu/frame
The track is 500 bu wide, and there are faces every 2 bu = 250 face. Each
face has 4 verts = 1000 verts per frame.

Can you move 1000 verts per frame in CPU and recalc their normals?
Experimentally: yes. Fine at far above playable speeds on my laptop

Can we cut down CPU by:
 - Don't move ones at zero position (tested with negligible effect)


Todo:
 - Rolling wave out from camera when terrain changes
'''

WORLD_SCALE = 2  # bu per square - must match the mesh
BEHIND = 5
TOTAL = 200
NUM_VERTS = 640  # number of verts in a single segment - for optimal cache size

import functools
import time
import random
import bge
import math
import mathutils


class Map(object):
    '''The environment that the player is driving through'''
    def __init__(self, scene, player_transform):
        self.valid = True
        self.scene = scene
        self._pieces = list()
        self._clear_time = time.time() - 1.0
        self.generator = MapGenerator(1234)

        self.player_follow_objs = [
            FollowPlayer(o, player_transform.translation) for o in
            scene.objects
            if 'FOLLOW_PLAYER' in o
        ]
        for i in range(TOTAL):
            self._add_piece(i)
        self.physics_mesh = Piece(self.scene.objects['PHYSICS_MESH'], new_mesh=False, physics=True)
        self.physics_mesh.on_offset_change.append(self.generator.generate_piece)
        self.physics_mesh.obj.worldPosition.z = -0.2

    def _add_piece(self, position_index):
        '''Creates a new land chunk'''
        if not self.valid:
            return
        new_obj = self.scene.addObject('GROUNDSECTION')
        piece = Piece(new_obj, new_mesh=True, physics=False)
        piece.on_offset_change.append(self.generator.generate_piece)
        piece.offset = position_index
        self._pieces.append(piece)

    def update(self, transform):
        '''Sets the player position so that old pieces can be cleared'''
        if not self.valid:
            return

        for obj in self.player_follow_objs:
            obj.update(transform)

        position = transform.translation

        # Find the tile of the player
        pos_id = int(position.y / WORLD_SCALE)
        time_diff = (time.time() - self._clear_time)
        
        # Move the physics mesh
        self.physics_mesh.x_pos = int(position.x / WORLD_SCALE)
        self.physics_mesh.offset = pos_id

        # Add any pieces needed
        for piece in self._pieces:
            if piece.offset < pos_id - BEHIND:
                piece.offset += TOTAL

            diff = piece.offset - pos_id  #3.19
            piece.height = self.get_height(diff, time_diff)  # 0.99

        self.physics_mesh.height = self.get_height(0, time_diff)

    def get_height(self, offset, time_diff):
        # This is currentlty one of the slower functions. Needs a speedup
        percent = (offset / TOTAL)
        ripple_location = time_diff / TOTAL * 100 + 0.01  # the addition avoids div0 issue
        grow = (1.0 - percent) #** 0.3  # Appear slowly on the horizon
        regrow = min(1, max(0, (percent/ripple_location - 1)))  # Dissapear in a wave
        regrow += min(1, max(0, (ripple_location + percent - 2 )))  # Regrow after destruction
        return grow * regrow + 0.0001
        
    @property
    def _add_pieces(self):
        return time.time() - self.clear_time > 1.0

    def clear_board(self):
        self._clear_time = time.time()

    def end(self):
        self.valid = False
        for lib in bge.logic.LibList():
            bge.logic.LibFree(lib)


class Piece(object):
    _next_id = 0
    def __init__(self, obj, new_mesh=True, physics=False):
        self.valid = True
        self.on_offset_change = list()
        
        self.obj = obj

        self.id = self._next_id
        Piece._next_id += 1
        self._libname = None

        self.update_physics = physics
            
        if new_mesh:
            self._libname = 'PIECE{}'.format(self.id)
            mesh_name = self.obj.meshes[0].name
            new_mesh = bge.logic.LibNew(self._libname, 'Mesh', [mesh_name])[0]
            self.obj.replaceMesh(new_mesh)
            self.mesh = new_mesh
        else:
            self.mesh = self.obj.meshes[0]

        self.obj.worldPosition.x = 0
        self.obj.worldPosition.z = 0

        self.verts = self._generate_verts()
        self.polygons = self._generate_polygons()
        self._offset = 0

    def _generate_verts(self):
        return tuple([
            self.mesh.getVertex(0, v)
            for v in range(self.mesh.getVertexArrayLength(0))
            ])
    def _generate_polygons(self):
        polygons = list()
        for poly_id in range(self.mesh.numPolygons):
            poly = self.mesh.getPolygon(poly_id)
            v1 = self.verts[poly.v1]
            v2 = self.verts[poly.v2]
            v3 = self.verts[poly.v3]
            if poly.v4 == 0:
                polygons.append((v1, v2, v3))
            else:
                v4 = self.verts[poly.v4]
                polygons.append((v1, v2, v3, v4))
        return tuple(polygons)

    def mesh_updated(self):
        '''Runs when the mesh was updated'''
        if self.update_physics:
            self.obj.reinstancePhysicsMesh()
        else:
            self.recalc_normals()

    def recalc_normals(self):
        '''recalcs the normals for a piece'''
        for poly in self.polygons:
            
            normal = mathutils.geometry.normal(*[p.getXYZ() for p in poly])
            for vert in poly:
                vert.setNormal(normal)

    @property
    def height(self):
        return self.obj.localScale.z

    @height.setter
    def height(self, val):
        self.obj.localScale.z = val

    @property
    def offset(self):
        return self._offset

    @offset.setter
    def offset(self, val):
        self._offset = val
        self.obj.worldPosition.y = val * WORLD_SCALE
        for funct in self.on_offset_change:
            funct(self, val)

    @property
    def x_pos(self):
        return self.obj.worldPosition.x / WORLD_SCALE

    @x_pos.setter
    def x_pos(self, val):
        self.obj.worldPosition.x = val * WORLD_SCALE

    def end(self):
        self.valid = False
        if self._libname is not None:
            bge.logic.LibFree(self._libname)


class FollowPlayer(object):
    '''Follows the player'''
    def __init__(self, obj, current_pos):
        self.offset = obj.worldPosition - current_pos
        self.obj = obj

    def update(self, current_pos):
        self.obj.worldPosition = current_pos.translation + self.offset
        if 'SNAP_TO_GRID' in self.obj:
            self.obj.worldPosition.xy = [int(i/2)*2 for i in self.obj.worldPosition.xy]
        if 'PLAYER_FACING' in self.obj:
            eul = current_pos.to_euler()
            eul.x = 0
            eul.y = 0
            self.obj.worldOrientation = eul



class MapGenerator(object):
    def __init__(self, seed):
        self.seed = seed
    
    def generate_piece(self, piece, offset):
        '''Moves the verts'''
        self.move_verts(piece)
        piece.mesh_updated()

    def move_verts(self, piece):
        offset = piece.offset * WORLD_SCALE
        x_pos = piece.x_pos * WORLD_SCALE
        get_point = self.get_point
        for vert in piece.verts:
            # Could do int() on each of these, but it doesn't seem to be
            # needed. The cache is still working properly
            vert_x = x_pos + vert.x
            vert_y = offset + vert.y
            pos = get_point(vert_x, vert_y)
            vert.z = pos

        # Max is about a 3:1 hit ratio because there are 4 verts at each point
        # info = self.get_point.cache_info()
        # print(info.hits / info.misses)
        

    @functools.lru_cache(maxsize=NUM_VERTS * 2)
    def get_point(self, x, y):
        #val = (x * x) / 500 + math.sin(y/10) * 2 - 20
        val = mathutils.noise.hetero_terrain([x/30, y/30, 1],
            0.5,
            3.5,
            #3.5 - abs(x)/40,
            2.5,
            #abs(x**1.2) / 500 + 0.5,
            abs(x**2.0) / 50000 + 0.0,
        ) * 20
        return max(0.0,  val - 10.0) * 10
        