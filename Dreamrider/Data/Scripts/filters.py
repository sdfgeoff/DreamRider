import bge

BOOST_CONSTANT_CA = 0.005
BOOST_INITIAL_CA = 0.05


class SpeedFilter(object):
    '''This creates the speed filter. Be aware that because this supports
    normal BGE it needs to fetch ... a controller and actuator'''

    def __init__(self, scene, pass_num):
        self.scene = scene  # not used atm
        self.pass_num = pass_num
        cont = bge.logic.getCurrentController()
        cont.owner['streak_length'] = 3.0
        cont.owner['streak_center_y'] = 0.5
        cont.owner['streak_center_x'] = 0.5
        cont.owner['streak_offset'] = 0.0
        cont.owner['chromatic_abbr'] = 0.0

        act = cont.actuators['Filter 2D']
        act.shaderText = SPEED_FILTER
        act.passNumber = self.pass_num
        cont.activate(act)

        self.boost_lag = 0
        self.boost_quick = 0

    def cb_on_player_move(self, player, boost):
        cont = bge.logic.getCurrentController()
        cont.owner['streak_offset'] += 0.1
        vel = player.obj.worldLinearVelocity
        cont.owner['streak_length'] = vel.y / 50
        cont.owner['streak_center_y'] = 0.5
        cont.owner['streak_center_x'] = 0.5 + vel.x / 1000

        self.boost_lag = boost * 0.05 + self.boost_lag * 0.95
        self.boost_quick = boost * 0.5 + self.boost_quick * 0.5
        chroma = max(0, boost - self.boost_lag)
        chroma_quick = max(0, boost - self.boost_quick)
        cont.owner['chromatic_abbr'] = boost * BOOST_CONSTANT_CA + \
            chroma * BOOST_INITIAL_CA - chroma_quick * BOOST_INITIAL_CA


SPEED_FILTER = '''
#version 120
uniform sampler2D bgl_RenderedTexture;
uniform sampler2D bgl_DepthTexture;

#define STREAKS 1000.0

uniform float streak_length = 3.0;
uniform float zoom_amt = 1.0;
uniform float chromatic_abbr = 0.01;
uniform float streak_center_y = 0.5;
uniform float streak_center_x = 0.5;

uniform float streak_offset = 0.0;

float rand(float num){
    return 0.5 + 0.5 * fract(sin(num * 78.233) * 437585.5453);
}

float getangle(vec2 coords){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    return float(int(atan(normalized.x / normalized.y) * STREAKS));
}

vec2 noisyzoom(vec2 coords, float zoom, float depth){
    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);
    float dist = length(normalized);

    // Streaks:
    float streaks = rand(getangle(coords)+streak_offset);
    streaks *= pow(dist, 2.0);
    streaks *= streak_length;
    streaks *= depth;
    normalized *= 1.0 - streaks;

    // Zoom:
    normalized *= zoom;

    return normalized + vec2(streak_center_x, streak_center_y);
}


void main(void){
    vec2 orig_coord = gl_TexCoord[0].xy;

    float depth = clamp(1.0 - pow(texture2D(bgl_DepthTexture, orig_coord).r, 8.0), 0.0, 1.0);

    vec2 texcoord = clamp(noisyzoom(orig_coord, zoom_amt, depth), vec2(-1.0), vec2(1.0));
    gl_FragColor.r = texture2D(bgl_RenderedTexture, texcoord).r + (depth*0.4);

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr, depth);
    gl_FragColor.g = texture2D(bgl_RenderedTexture, texcoord).g;

    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr * 2.0, depth);
    gl_FragColor.b = texture2D(bgl_RenderedTexture, texcoord).b;
}
'''
