class HUD(object):
    '''AN overlay to provide information to the user'''
    def __init__(self, scene):
        self.scene = scene

    def set_speed(self, val):
        '''Little red numbers in the bottom. Expects a number less than 999'''
        self.scene.objects['SPEED'].text = "{:03}".format(int(val))
        for obj in self.scene.objects:
            if 'SPEED_PERCENT' in obj:
                obj.color = [val / 500] * 4


    def set_percent(self, val):
        '''Sets the percent of the level progress bar'''
        self.scene.objects['PERCENT'].color = [val] * 4

    def set_name(self, name):
        '''Sets the line of text at teh top of the screen'''
        self.scene.objects['NAME'].text = name

    def cb_player_shield(self, bank, amt):
        self.scene.objects['SHIELD'].color = [bank.percent, 0, 0, 1]

    def set_fade(self, percent):
        self.scene.objects['OVERLAY'].color = [0, 0, 0, percent]