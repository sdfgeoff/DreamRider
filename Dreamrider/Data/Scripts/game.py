import random
import os
import json

import bge
import aud
import mutagen

import player
import hud
import world
import filters

import cProfile
PROFILE = True

def init(cont):
    if 'COUNTER' not in cont.owner:
        cont.owner['COUNTER'] = 0
        bge.logic.addScene('HUD')
    cont.owner['COUNTER'] += 1
    if cont.owner['COUNTER'] == 2:
        cont.owner['GAME'] = Game(cont.owner.scene)
        if PROFILE:
            cont.script = __name__ + '.run_profiled'
            cont.owner['PROFILER'] = cProfile.Profile()
        else:
            cont.script = __name__ + '.run'


def run(cont):
    cont.owner['GAME'].update()

def run_profiled(cont):
    cont.owner['PROFILER'].runcall(cont.owner['GAME'].update)
    
    if bge.events.ENTERKEY in bge.logic.keyboard.active_events:
        cont.owner['PROFILER'].print_stats(sort='cumtime')


class Game(object):
    def __init__(self, scene):
        self.scene = scene
        self.player = player.Player(scene.objects['PLAYER'])

        folder = bge.logic.expandPath('//../../Tracks')
        tracks = [
            o for o in
            os.listdir(folder)
            if o.split('.')[-1] in ['mp3', 'wav']
        ]
        if tracks:
            track = os.path.join(folder, random.choice(tracks))
        else:
            track = None
        self.music = Music(track)
        self.hud = hud.HUD([s for s in bge.logic.getSceneList() if s.name == 'HUD'][0])

        self.player.shield.on_empty.append(self.end)
        self.player.shield.on_change.append(self.hud.cb_player_shield)
        self.hud.cb_player_shield(self.player.shield, 0)
        self.player.on_hit.append(self.on_player_hit)

        self.map = world.Map(self.scene, self.player.transform)
        self.filter = filters.SpeedFilter(self.scene, 0)
        self.player.on_move.append(self.filter.cb_on_player_move)

        self.counter = 0

    def update(self):
        '''Links the HUD to the player'''
        self.player.update()
        self.map.update(self.player.transform)
        self.hud.set_speed(self.player.current_speed)
        self.hud.set_percent(self.music.percent)

        if self.music.percent >= 0.99999:
            self.music.end()
            for scene in bge.logic.getSceneList():
                scene.end()
            bge.logic.addScene('COMPLETE')

        self.counter += 1
        name_str = self.music.name[max(0, int(self.counter/3) - 80):int(self.counter/3)]
        self.hud.set_name(name_str)
        self.hud.set_fade(1 - min(1, self.counter/60))

        # if self.counter %2 == 0:
        #     bge.render.makeScreenshot('dreamrider{:04}.jpg'.format(int(self.counter/2)))

    def on_player_hit(self, *_args):
        self.map.clear_board()

    def end(self, *_args):
        '''Ends the game'''
        self.music.end()
        for scene in bge.logic.getSceneList():
            scene.end()
        bge.logic.addScene('FAILED')
        self.map.end()



class TrackData(object):
    def __init__(self, file_path):
        self.file_path = file_path
        json_path = self.file_path.rsplit('.', 1)[0] + '.trackdata'
        try:
            self.data = json.load(open(json_path, 'r'))
        except:
            self.data = self._generate_data()
            json.dump(self.data, open(json_path, 'w'))

    def _generate_data(self):
        '''Extracts information from the audio file'''
        file_info = mutagen.File(self.file_path)
        data = dict()
        try:
            data['name'] = str(file_info['TIT2'])
            data['artist'] = str(file_info['TPE1'])
        except:
            data['name'] = os.path.split(self.file_path)[1].split('.')[0]
            data['artist'] = 'unknown'

        # Measure duration - the slow way
        device = aud.device()
        music = aud.Factory(self.file_path)
        handle = device.play(music)

        at_end = False
        duration = 0.0
        while not at_end:
            duration += 60.0
            handle.position = duration
            while handle.position != duration:
                duration -= 1.0
                handle.position = duration
                at_end = True
        data['duration'] = duration

        return data

    def __getattr__(self, name):
        return self.data[name]


class Music(object):
    '''Plays the soundtrack'''
    def __init__(self, path):
        self.device = aud.device()
        self.file_path = path
        if self.file_path is None:
            self.name = 'No Tracks'
            return
        self.music = aud.Factory(self.file_path)
        self.handle = self.device.play(self.music)

        self.data = TrackData(self.file_path)
        self.name = self.data.name + ' - ' + self.data.artist
        self.duration = self.data.duration

    @property
    def percent(self):
        '''Returns how far through the song it is'''
        if self.file_path is None:
            return 0
        return self.handle.position / self.duration

    def end(self):
        '''Stops the music playing'''
        if self.file_path is None:
            return
        self.handle.stop()
