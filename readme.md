[![Dreamrider](https://img.youtube.com/vi/in7mbJEIAdY/0.jpg)](https://www.youtube.com/watch?v=in7mbJEIAdY)

Dreamrider is an infinite running music game in the style of an 80's vector
graphics display. I know that in the 80's, games didn't actually look like
this, but hey!

Stick your own music in the 'Tracks' folder to drive to your own tunes.


Dependencies
------------
Developed on blender 2.78, but may switch to UPBGE in the near future for a
better land scape generator.